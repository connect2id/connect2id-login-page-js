Connect2id Login Page (JavaScript)

Copyright (c) Connect2id Ltd., 2013 - 2015


README

Connect2id login page implemented in JavaScript. Use only for testing and
development purposes.

Demonstrates the following:

 * Simple authentication and consent forms for OpenID Connect.

 * Implementation of LDAP user authentication using the Connect2id LdapAuth
   JSON-RPC 2.0 service.

 * Use of the authorisation session endpoint of the Connect2id server to
   decode the OpenID Connect request, present the required forms, and return
   the encoded response to the client application (relying party).

 * Use of the session store endpoint of the Connect2id server to display
   information about the user, also to facilitate logout.


Compatibility: 

 * Connect2id Server 3.3+
 

For more information: http://connect2id.com/products/server


Questions or comments? Email support@connect2id.com
